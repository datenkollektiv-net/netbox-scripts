import requests
from xml.etree import ElementTree

from netbox.models import NetBoxModel
from netbox_dns.models import View, Zone, NameServer, Record

from extras.scripts import Script, StringVar


class Hetzner():
    def __init__(self, api_token: str):
        self.api_token = api_token

    def api_get_zones(self, page: int = 1, per_page: int = 100,
                      search_name: str = None) -> requests.Response:

        # https://dns.hetzner.com/api-docs#operation/GetZones
        return requests.get(
            url="https://dns.hetzner.com/api/v1/zones",
            headers={
                "Auth-API-Token": self.api_token,
            },
            params={
                "page": page,
                "per_page": per_page,
                "search_name": search_name,
            },
        )

    def api_get_records(self, zone_id: str = None) -> requests.Response:

        # https://dns.hetzner.com/api-docs#tag/Records
        return requests.get(
            url="https://dns.hetzner.com/api/v1/records",
            headers={
                "Auth-API-Token": self.api_token,
            },
            params={
                "zone_id": zone_id,
            },
        )

    def get_all_zones(self, search_name: str = None) -> list:
        # ask for first page save page metadata
        req = self.api_get_zones(page=1, search_name=search_name).json()
        pagination = req['meta']['pagination']
        total_entries = pagination['total_entries']

        # save requested zones
        zones = req['zones']

        # requests all other zones
        while (pagination['page'] != pagination['last_page']):
            req = self.api_get_zones(page=pagination['next_page'], search_name=search_name).json()

            zones = zones + req['zones']
            pagination = req['meta']['pagination']

        assert len(zones) == total_entries

        return zones


class Helper():
    @staticmethod
    def parse_soa(nb_script: Script, req_record: str) -> dict:
        values = req_record['value'].split(' ')
        return {
            'mname': Helper.get_nameserver(nb_script, values[0]),
            'rname': values[1],
            'serial': values[2],
            'refresh': values[3],
            'retry': values[4],
            'expire': values[5],
            'minimum': values[6],
            'ttl': req_record.get('ttl'),
        }

    def get_nameserver(nb_script: Script, name: str) -> NameServer:
        # only lowercase characters
        name = name.lower()
        if not name.endswith('.'):
            name = name + '.'

        nameserver, created = NameServer.objects.get_or_create(name=name)
        if created:
            nameserver.tags.add(nb_script.tag)
            nb_script.log_warning(f"Created new NS: {nameserver}")

        Helper.add_tag(nb_script, nameserver)
        return nameserver

    @staticmethod
    def update_or_create_zone(nb_script: Script, view: str, name: str, zone_records: dict) -> None:
        # only lowercase characters
        name = name.lower()

        if zone_records['soa'].get('ttl') is None:
            soa_ttl = zone_records['default_ttl']
        else:
            soa_ttl = zone_records['soa']['ttl']

        try:
            zone = Zone.objects.get(view=view, name=name)
        except Zone.DoesNotExist:
            zone = Zone.objects.create(view=view,
                                       name=name,
                                       default_ttl=zone_records['default_ttl'],
                                       soa_ttl=soa_ttl,
                                       soa_mname=zone_records['soa']['mname'],
                                       soa_rname=zone_records['soa']['rname'],
                                       soa_serial=zone_records['soa']['serial'],
                                       soa_refresh=zone_records['soa']['refresh'],
                                       soa_retry=zone_records['soa']['retry'],
                                       soa_expire=zone_records['soa']['expire'],
                                       soa_minimum=zone_records['soa']['minimum'])
            nb_script.log_warning(f"Created new Zone: {zone}")

        zone.nameservers.set(zone_records['nameserver'])
        zone.save()

        Helper.add_tag(nb_script, zone)
        nb_script.log_warning(f"Update Zone: {zone}")

        # Create or update other records related to this zone
        if zone_records.get('others') is not None:
            for zone_record in zone_records['others']:
                Helper.update_or_create_record(
                    nb_script, zone, zone_record, zone_records['default_ttl'])

    @staticmethod
    def update_or_create_record(nb_script: Script, zone: Zone, zone_record: dict, default_ttl: str) -> None:
        # only lowercase characters
        zone_record['name'] = zone_record['name'].lower()
        zone_record['type'] = zone_record['type'].lower()

        try:
            record = Record.objects.get(zone=zone,
                                        type=zone_record['type'],
                                        name=zone_record['name'])
            record.name = zone_record['name'],
            record.type = zone_record['type'],
            record.value = zone_record['value'],
            record.ttl = zone_record.get('ttl', default_ttl)
            record.save

        except Record.DoesNotExist:
            record = Record.objects.create(
                zone=zone,
                name=zone_record['name'],
                type=zone_record['type'],
                value=zone_record['value'],
                ttl=zone_record.get('ttl', default_ttl))

            Helper.add_tag(nb_script, record)
            nb_script.log_warning(f"Created new Record: {record}")

    @staticmethod
    def get_world_view(nb_script: Script) -> View:
        world_view, created = View.objects.get_or_create(name="World")
        if created:
            nb_script.log_warning(f"Created new View: {world_view}")

        Helper.add_tag(nb_script, world_view)
        return world_view

    @staticmethod
    def add_tag(nb_script: Script, obj: NetBoxModel) -> None:
        if nb_script.tag not in obj.tags.names():
            # Add zone and provide diff changelog
            if obj.pk and hasattr(obj, 'snapshot'):
                obj.snapshot()
            obj.tags.add(nb_script.tag)
            obj.full_clean()
            obj.save()
            nb_script.log_warning(f"Add tag to NetBoxModel: {obj}")


class HetznerSync(Script):
    class Meta:
        name = "Hetzner Sync (only Pull)"
        description = "Pull DNS entries stored at Hetzner DNS (no merge or deletion)"
        field_order = ['api_token', 'zone']
        commit_default = True
        job_timeout = 1200  # 20 min, cause it may take a while

    api_token = StringVar(
        label="Hetzner API Token",
        description="Hetzner Auth-API-Token for DNS Public API",
        required=True
    )

    zone = StringVar(
        label="Zone",
        description="Partial name of a zone. Will return zones that contain the searched string."
        "Leave empty to get all zones.",
        required=False
    )

    tag = StringVar(
        description="Tag that is added to new objects.",
        default="Hetzner",
        regex="\\w+",  # only a single word is allowed
        required=False
    )

    def run(self, data, commit):
        #  Little disclaimer
        self.log_info("Removing deleted/non-existing objects is not implemented yet and currently "
                      "no intelligent update routine is implemented, only overwriting the entire "
                      "object.")

        # Read provided script data
        hetzner = Hetzner(data['api_token'])
        req_zones = hetzner.get_all_zones(search_name=data['zone'])
        self.tag = data['tag']  # overwrite extras.scripts.StringVar object

        # Create or get "World" view
        world_view = Helper.get_world_view(self)

        # Start iterating over all zones
        for req_zone in req_zones:
            # Initialize records before collecting them
            zone_records = {
                'default_ttl': req_zone['ttl'],
                'soa': {},
                'nameserver': [],
                'others': []
            }

            # Requests records for current zone and handle different record types
            req_records = hetzner.api_get_records(zone_id=req_zone['id']).json()['records']
            for req_record in req_records:
                if req_record['type'] == 'SOA':
                    zone_records['soa'] = Helper.parse_soa(self, req_record)
                elif req_record['type'] == 'NS':
                    nameserver = Helper.get_nameserver(self, req_record['value'])
                    zone_records['nameserver'].append(nameserver)
                elif zone_records.get('others') is not None:
                    zone_records['others'].append({'type': req_record['type'],
                                                   'name': req_record['name'],
                                                   'value': req_record['value']})

            # Everything should be now collected, start creating zone including related records
            Helper.update_or_create_zone(self, world_view, req_zone['name'], zone_records)


class Namecheap():
    xmlns = {'xmlns': "http://api.namecheap.com/xml.response"}

    def __init__(self, username: str, api_key: str, client_ip: str):
        self.username = username
        self.api_key = api_key
        self.client_ip = client_ip

    def get_domains(self, page: int = 1, page_size: int = 10) -> list:
        #  https://www.namecheap.com/support/api/methods/domains/get-list/
        req = requests.get(
            url="https://api.namecheap.com/xml.response",
            params={
                'ApiUser': self.username,
                'ApiKey': self.api_key,
                'UserName': self.username,
                'Command': 'namecheap.domains.getList',
                'ClientIp': self.client_ip,
                'Page': page,
                'PageSize': page_size
            }
        )

        root = ElementTree.fromstring(req.content)
        return [domain.attrib['Name'] for domain in root.findall('.//xmlns:Domain', self.xmlns)]

    def get_dns_info(self, domain: str) -> dict:
        # https://www.namecheap.com/support/api/methods/domains-dns/get-list/
        req = requests.get(
            url="https://api.namecheap.com/xml.response",
            params={
                'ApiUser': self.username,
                'ApiKey': self.api_key,
                'UserName': self.username,
                'Command': 'namecheap.domains.dns.getList',
                'ClientIp': self.client_ip,
                'SLD': domain.split('.')[:-1],
                'TLD': domain.split('.')[-1:]
            }
        )
        root = ElementTree.fromstring(req.content)
        dns_node = root.find('./xmlns:CommandResponse/xmlns:DomainDNSGetListResult', self.xmlns)
        return {'name': domain,
                'namecheap_managed': dns_node.attrib['IsUsingOurDNS'].lower() == 'true',
                'nameservers': [nameserver.text for nameserver in
                                dns_node.findall('./xmlns:Nameserver', self.xmlns)]
                }


class NamecheapSync(Script):
    class Meta:
        name = "Namecheap Sync (only Pull)"
        description = "Pull DNS entries stored at Namecheap (no merge or deletion)"
        field_order = ['username', 'api_key', 'client_ip']
        commit_default = True

    username = StringVar(
        label="Username",
        description="Username for DNS API",
        required=True
    )

    api_key = StringVar(
        label="API key",
        description="Namecheap API key for DNS API",
        required=True
    )

    client_ip = StringVar(
        label="Client IPv4",
        description="Client IPv4 from where a request is allowed (Public IPv4 of this host)",
        required=True
    )

    tag = StringVar(
        description="Tag that is added to new objects.",
        default="Namecheap",
        regex="\\w+",  # only a single word is allowed
        required=False
    )

    def run(self, data, commit):
        # Read provided script data
        namecheap = Namecheap(data['username'], data['api_key'],  data['client_ip'])
        self.tag = data['tag']  # overwrite extras.scripts.StringVar object

        # Create or get "World" view
        world_view = Helper.get_world_view(self)

        # Get all zones from API
        domains = namecheap.get_domains(page_size=100)

        for domain in domains:
            domain = namecheap.get_dns_info(domain)
            if domain['namecheap_managed']:
                # Create static records, because not all is available via API
                zone_records = {
                    'soa': {
                        'mname': Helper.get_nameserver(self, 'dns1.registrar-servers.com'),
                        'rname': 'hostmaster.registrar-servers.com.',
                        'serial': '1573506107',
                        'refresh': '43200',
                        'retry': '3600',
                        'expire': '604800',
                        'minimum': '3601',
                    },
                    'default_ttl': '3600',
                    'nameserver': [Helper.get_nameserver(self, ns) for ns in domain['nameservers']],
                }

                zone = Helper.update_or_create_zone(self, world_view, domain['name'], zone_records)
            else:
                try:
                    zone = Zone.objects.get(view=world_view, name=domain['name'])
                    Helper.add_tag(self, zone)
                except Zone.DoesNotExist:
                    self.log_failure(f"Zone not found: {domain['name']}")


class Namesilo():
    def __init__(self, api_key: str):
        self.api_key = api_key

    def get_domains(self, page: int = 1, page_size: int = 10) -> list:
        req = requests.get(
            # https://www.namesilo.com/api-reference#domains/list-domains
            url="https://www.namesilo.com/api/listDomains",
            params={
                'version': 1,
                'type': "xml",
                'key': self.api_key,
                'page': page,
                'pageSize': page_size
            }
        )

        root = ElementTree.fromstring(req.content)
        return [domain.text for domain in root.findall('./reply/domains/domain')]

    def get_dns_info(self, domain: str) -> dict:
        req = requests.get(
            # https://www.namesilo.com/api-reference#domains/get-domain-info
            url="https://www.namesilo.com/api/getDomainInfo",
            params={
                'version': 1,
                'type': "xml",
                'key': self.api_key,
                'domain': domain
            }
        )

        root = ElementTree.fromstring(req.content)
        return {'name': domain,
                'namesilo_managed': root.find('./reply/traffic_type').text != 'Custom DNS',
                'nameservers': [nameserver.text for nameserver in
                                root.findall('./reply/nameservers/nameserver')]
                }

    def get_records(self, domain: str) -> list:
        req = requests.get(
            # https://www.namesilo.com/api-reference#dns/dns-list-records
            url="https://www.namesilo.com/api/dnsListRecords",
            params={
                'version': 1,
                'type': "xml",
                'key': self.api_key,
                'domain': domain
            }
        )

        root = ElementTree.fromstring(req.content)
        records = []
        for record in root.findall('./reply/resource_record'):
            # Fix record name for Netbox
            name = record.find('./host').text.replace(domain, '')
            if name == '':
                name = '@'
            elif name.endswith('.'):
                name = name[:-1]

            records.append({
                'type': record.find('./type').text,
                'name': name,
                'value': record.find('./value').text,
                'ttl': record.find('./ttl').text
            })
        return records


class NamesiloSync(Script):
    class Meta:
        name = "Namesilo Sync (only Pull)"
        description = "Pull DNS entries stored at Namesilo (no merge or deletion)"
        field_order = ['api_key']
        commit_default = True

    api_key = StringVar(
        label="API key",
        description="Namesile API key for DNS API",
        required=True
    )

    tag = StringVar(
        description="Tag that is added to new objects.",
        default="Namesilo",
        regex="\\w+",  # only a single word is allowed
        required=False
    )

    def run(self, data, commit):
        # Read provided script data
        namesilo = Namesilo(data['api_key'])
        self.tag = data['tag']  # overwrite extras.scripts.StringVar object

        # Create or get "World" view
        world_view = Helper.get_world_view(self)

        # Get all zones silo API
        domains = namesilo.get_domains(page_size=100)

        for domain in domains:
            domain = namesilo.get_dns_info(domain)
            if domain['namesilo_managed']:
                # Create static records, because not all is available via API
                zone_records = {
                    'soa': {
                        'mname': Helper.get_nameserver(self, 'ns1.dnsowl.com.'),
                        'rname': 'hostmaster.dnsowl.com.',
                        'serial': '1675723864',
                        'refresh': '7200',
                        'retry': '1800',
                        'expire': '1209600',
                        'minimum': '60',
                    },
                    'default_ttl': '172800',
                    'nameserver': [Helper.get_nameserver(self, ns) for ns in domain['nameservers']],
                    'others': namesilo.get_records(domain['name'])
                }

                zone = Helper.update_or_create_zone(self, world_view, domain['name'], zone_records)
            else:
                try:
                    zone = Zone.objects.get(view=world_view, name=domain['name'])
                    Helper.add_tag(self, zone)
                except Zone.DoesNotExist:
                    self.log_failure(f"Zone not found: {domain['name']}")


class CleanupAll(Script):
    class Meta:
        name = "Cleanup DNS"
        description = "Delete all related DNS objects"
        commit_default = True

    def run(self, data, commit):
        Record.objects.all().delete()
        self.log_success('Cleanup DNS "Record" objects')

        Zone.objects.all().delete()
        self.log_success('Cleanup DNS "Zone" objects')

        NameServer.objects.all().delete()
        self.log_success('Cleanup DNS "NameServer" objects')

        View.objects.all().delete()
        self.log_success('Cleanup DNS "View" objects')


script_order = (HetznerSync, NamecheapSync, NamesiloSync, CleanupAll)
