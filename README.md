# Custom Netbox scripts

## Contribution

### Development setup

**Get Netbox Docker**

```sh
git clone https://github.com/netbox-community/netbox-docker.git
cd netbox-docker
```

**Integrate this repo**

```sh
git submodule add git@gitlab.com:datenkollektiv-net/netbox-scripts.git
```

**Integrate DNS plugin**
( [Documentation "Using-Netbox-Plugins](https://github.com/netbox-community/netbox-docker/wiki/Using-Netbox-Plugins) )

```sh
# Prepare 
echo "netbox-dns" > plugin_requirements.txt
echo 'PLUGINS = ["netbox_dns"]' >> configuration/plugins.py

# Create new docker build script
cat <<EOF > Dockerfile-Plugins
FROM netboxcommunity/netbox:v3.4.2
COPY ./plugin_requirements.txt /
RUN /opt/netbox/venv/bin/pip install  --no-warn-script-location -r /plugin_requirements.txt
EOF

# Create docker-compose override 
cat <<EOF > docker-compose.override.yml
version: '3.4'
services:
  netbox:
    ports:
      - 8000:8080
    build:
      context: .
      dockerfile: Dockerfile-Plugins
    image: netbox:latest-plugins
    volumes:
      - ./netbox-scripts:/etc/netbox/scripts:z,ro
  netbox-worker:
    image: netbox:latest-plugins
    build:
      context: .
      dockerfile: Dockerfile-Plugins
    volumes:
      - ./netbox-scripts:/etc/netbox/scripts:z,ro
  netbox-housekeeping:
    image: netbox:latest-plugins
    build:
      context: .
      dockerfile: Dockerfile-Plugins
EOF

# Create new docker image and startup
docker-compose build --no-cache
docker-compose up -d
```

### Code Formatter
- `python3-autopep8`

## Useful websites

### Plugins
- [netbox-dns](https://github.com/auroraresearchlab/netbox-dns)

### Documentations
- [NetBox Documentation > Customization > Custom Scripts](https://docs.netbox.dev/en/stable/customization/custom-scripts/)
- Testing in python
  - [Getting Started With Testing in Python](https://realpython.com/python-testing/)
  - [Effective Python Testing With Pytest](https://realpython.com/pytest-python-testing/)

### Tutorials
- [NetBox - Zero To Hero Course](https://zerotohero.netbox.dev/)

### Other Git repositories (script examples)
- [https://github.com/melkypie/netbox-automation](https://github.com/melkypie/netbox-automation/tree/main/scripts)
